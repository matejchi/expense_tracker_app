
require 'rails_helper'
require 'pundit/rspec'


RSpec.describe ExpensePolicy do

	before(:all) do
		#role
		@admin_role = FactoryGirl.create(:admin_role)
		@regular_role = FactoryGirl.create(:regular_role)

		#user
		@admin_user = FactoryGirl.create(:admin_user)
		@regular_user = FactoryGirl.create(:regular_user)
		@another_user = FactoryGirl.create(:regular_user, :email => 'another@isp.net')
	end

	after(:all) do
		Role.destroy_all
		User.destroy_all
	end

	permissions :create? do
		it "should not allow creating expense if user is not admin or owner of expense" do
			expect(ExpensePolicy).not_to permit(@regular_user, FactoryGirl.create(:expense, :user_id => @admin_user.id))
			expect(ExpensePolicy).not_to permit(@regular_user, FactoryGirl.create(:expense, :user_id => @another_user.id))
		end

		it "should allow creating expense if user is admin or owner of expense" do
			expect(ExpensePolicy).to permit(@admin_user, FactoryGirl.create(:expense, :user_id => @admin_user.id))
			expect(ExpensePolicy).to permit(@admin_user, FactoryGirl.create(:expense, :user_id => @regular_user.id))
			expect(ExpensePolicy).to permit(@another_user, FactoryGirl.create(:expense, :user_id => @another_user.id))
		end
	end

	permissions :show? do
		it "should allow showing details of expense if user is admin" do
			FactoryGirl.create(:expense, :user_id => @admin_user.id)
			FactoryGirl.create(:expense, :user_id => @regular_user.id)
			expect(ExpensePolicy).to permit(@admin_user, Expense.find_by_user_id(@admin_user.id))
			expect(ExpensePolicy).to permit(@admin_user, Expense.find_by_user_id(@regular_user.id))
		end

		it "should not allow showing details of expense if regular user tries to see someone else's expense" do
			FactoryGirl.create(:expense, :user_id => @admin_user.id)
			expect(ExpensePolicy).not_to permit(@regular_user, Expense.find_by_user_id(@admin_user.id))
		end

		it "should allow showing details of expense if regular user tries to see his own expense" do
			FactoryGirl.create(:expense, :user_id => @regular_user.id)
			expect(ExpensePolicy).to permit(@regular_user, Expense.find_by_user_id(@regular_user.id))
		end
	end

	permissions :update? do
		it "should allow admin to update his own and someone else's expenses" do
			admin_expense = FactoryGirl.create(:expense, :user_id => @admin_user.id)
			regular_user_expense = FactoryGirl.create(:expense, :user_id => @regular_user.id)

			expect(admin_expense.description).to eq "some description"
			expect(ExpensePolicy).to permit(@admin_user, admin_expense.update_attributes(:description => "updated") )
			expect(admin_expense.description).to eq "updated"

			expect(regular_user_expense.description).to eq "some description"
			expect(ExpensePolicy).to permit(@admin_user, regular_user_expense.update_attributes(:description => "updated") )
			expect(regular_user_expense.description).to eq "updated"
		end
	end

	permissions :destroy? do
		it "should allow admin to delete his own expense" do
			admin_expense = FactoryGirl.create(:expense, :user_id => @admin_user.id)
			expect(ExpensePolicy).to permit(@admin_user, admin_expense)
		end

		it "should allow admin to delete someone else's expense" do
			expense = FactoryGirl.create(:expense)
			expect(ExpensePolicy).to permit(@admin_user, expense)
		end

		it "should allow regular user to delete his own expense" do
			expense = FactoryGirl.create(:expense)
			expect(ExpensePolicy).to permit(@regular_user, expense)
		end

		it "should not allow regular user to delete someone else's expense" do
			expense = FactoryGirl.create(:expense, :user_id => @another_user.id)
			expect(ExpensePolicy).not_to permit(@regular_user, expense)
		end
	end

end

