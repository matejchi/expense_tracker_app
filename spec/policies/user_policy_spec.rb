
require 'rails_helper'
require 'pundit/rspec'


RSpec.describe UserPolicy do

	before(:all) do
		#role
		@admin_role = FactoryGirl.create(:admin_role)
		@regular_role = FactoryGirl.create(:regular_role)

		#user
		@admin_user = FactoryGirl.create(:admin_user)
		@regular_user = FactoryGirl.create(:regular_user)
	end

	after(:all) do
		Role.destroy_all
		User.destroy_all
	end

	permissions :create? do
		it "should allow creating user for admin" do
			expect(UserPolicy).to permit(@admin_user, FactoryGirl.create(:regular_user, :email => "newemail@isp.net"))
		end

		it "should not allow creating new users for regular user" do
			expect(UserPolicy).not_to permit(@regular_user, FactoryGirl.create(:regular_user, :email => "someone@isp.net"))
		end
	end

	permissions :show? do
		it "should allow showing details of user if user is admin" do
			expect(UserPolicy).to permit(@admin_user, User.find_by_id(@regular_user.id))
			expect(UserPolicy).to permit(@admin_user, User.find_by_id(@admin_user.id))
		end

		it "should not allow showing details of another user if regular user tries to see someone else's user details" do
			expect(UserPolicy).not_to permit(@regular_user, User.find_by_id(@admin_user.id))
		end
	end

	permissions :update? do
		it "should allow admin to update his own and someone else's user profile" do
			admin_profile = User.find_by_id(@admin_user.id)
			regular_user_profile = User.find_by_id(@regular_user.id)

			expect(UserPolicy).to permit(@admin_user, admin_profile.update_attributes(:first_name => "updated") )
			expect(UserPolicy).to permit(@admin_user, regular_user_profile.update_attributes(:first_name => "updated") )
		end
	end

	permissions :destroy? do
		it "should allow admin to delete someone's account" do
			user = User.find_by_id(@regular_user.id)
			expect(UserPolicy).to permit(@admin_user, user)
		end

		it "should not allow regular user to delete his own account" do
			user = User.find_by_id(@regular_user.id)
			expect(UserPolicy).not_to permit(@regular_user, user)
		end

		it "should not allow regular user to delete someone else's account" do
			user = User.find_by_id(@admin_user.id)
			expect(UserPolicy).not_to permit(@regular_user, user)
		end
	end

	permissions :index? do
		it "should allow admin to see all accounts" do
			users = User.all
			expect(UserPolicy).to permit(@admin_user, users)
		end

		it "should not allow regular user to see all user accounts" do
			users = User.all
			expect(UserPolicy).not_to permit(@regular_user, users)
		end
	end

end