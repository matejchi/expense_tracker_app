require 'rails_helper'

RSpec.describe Api::V1::UsersController, type: :request do

	before(:all) do
		@admin_role = FactoryGirl.create(:admin_role)
		@regular_role = FactoryGirl.create(:regular_role)
		@admin_user = FactoryGirl.create(:admin_user)
		@regular_user = FactoryGirl.create(:regular_user)
	end

	after(:all) do
		Role.destroy_all
		User.destroy_all
	end


	#index
	it "should return status 200 for admin user (admin should be able to see all users)" do
		get "/api/users", nil, {"api-token" => @admin_user.api_token, "email" => @admin_user.email}
		expect(response.status).to eq 200
	end

	it "should return status 403 for regular user (regular_user shouldn't be able to see all users" do
		get "/api/users", nil, {"api-token" => @regular_user.api_token, "email" => @regular_user.email}
		expect(response.status).to eq 403
	end

	it "should return 3 user records for admin user (admin should see all users (3))" do
		@user = FactoryGirl.create(:regular_user, :email =>'test@isp.net')
		get "/api/users", nil, {"api-token" => @admin_user.api_token, "email" => @admin_user.email}
		returned_value = JSON.parse(response.body)
		expect(returned_value['users'].count).to eq 3
	end
	#------------------------------------------------------------------------------------------------------------------------------------------------------------

	#show
	it "should return status 200 for admin user (admin should be able to see details for each user)" do
		get "/api/users/2", nil, {"api-token" => @admin_user.api_token, "email" => @admin_user.email}
		expect(response.status).to eq 200
	end

	it "should return status 403 for regular user (regular user shouldn't be able to see other user's detail data)" do
		get "/api/users/1", nil, {"api-token" => @regular_user.api_token, "email" => @regular_user.email}
		expect(response.status).to eq 403
	end

	it "should return status 200 for regular user (regular user should be able to see it's own data)" do
		get "/api/users/2", nil, {"api-token" => @regular_user.api_token, "email" => @regular_user.email}
		expect(response.status).to eq 200
	end

	it "regular user's email should equal to returned email (regular user should be able to see it's own data)" do
		get "/api/users/2", nil, {"api-token" => @regular_user.api_token, "email" => @regular_user.email}
		returned_value = JSON.parse(response.body)
		expect(returned_value['user']['email']).to eq @regular_user.email
	end
	#------------------------------------------------------------------------------------------------------------------------------------------------------------

	#destroy
	it "admin should be able to delete user" do
		delete "/api/users/2", nil, {"api-token" => @admin_user.api_token, "email" => @admin_user.email}
		expect(response.status).to eq 200
	end

	it "regular user shouldn't be able to delete user" do
		delete "/api/users/2", nil, {"api-token" => @regular_user.api_token, "email" => @regular_user.email}
		expect(response.status).to eq 403
	end

	it "admin should get message when user is deleted" do
		@user = FactoryGirl.create(:regular_user, :email => 'blabla@isp.net')
		delete "/api/users/#{@user.id}", nil, {"api-token" => @admin_user.api_token, "email" => @admin_user.email}
		returned_value = JSON.parse(response.body)
		expect(returned_value["message"]).to eq "User deleted"
	end
	#------------------------------------------------------------------------------------------------------------------------------------------------------------

	#create
	it "should return status 201 for admin user" do
		post "/api/users", {:first_name => 'Bruce', :last_name => 'Lee', :email => 'test@isp.net', :password => '0000', :password_confirmation => '0000'}, {"api-token" => @admin_user.api_token, "email" => @admin_user.email}
		returned_value = JSON.parse(response.body)
		expect(response.status).to eq 201
		expect(returned_value['message']).to eq "User successfully created"
	end

	it "should return status 409 and error message for admin user" do
		post "/api/users", {:first_name => 'Bruce', :last_name => 'Lee', :email => 'admin@isp.net', :password => '0000', :password_confirmation => '0000'}, {"api-token" => @admin_user.api_token, "email" => @admin_user.email}
		returned_value = JSON.parse(response.body)
		expect(response.status).to eq 409
		expect(returned_value['message']).to eq "Email has already been taken"
	end

	it "should return status 201 for regular user (regular user should be able to create its own account)" do
		post "/api/users", {:first_name => 'Bruce', :last_name => 'Lee', :email => 'newuser@isp.net', :password => '0000', :password_confirmation => '0000'}, nil
		expect(response.status).to eq 201
	end
	#------------------------------------------------------------------------------------------------------------------------------------------------------------

	#update
	it "should return status 200 for admin user" do
		@user = FactoryGirl.create(:regular_user, :email => 'third_user@isp.net')
		put "/api/users/#{@user.id}", {:email => 'test@isp.net'}, {"api-token" => @admin_user.api_token, "email" => @admin_user.email}
		expect(response.status).to eq 200
	end

	it "should return changed email when admin user update another one" do
		@user = FactoryGirl.create(:regular_user, :email => 'third_user@isp.net')
		put "/api/users/#{@user.id}", {:email => 'test@isp.net'}, {"api-token" => @admin_user.api_token, "email" => @admin_user.email}
		returned_value = JSON.parse(response.body)
		expect(returned_value['user']['email']).to eq 'test@isp.net'
	end

	it "regular user shouldn't be able to update someone else's profile" do
		@user = FactoryGirl.create(:regular_user, :email => 'third_user@isp.net')
		put "/api/users/#{@user.id}", {:email => 'test@isp.net'}, {"api-token" => @regular_user.api_token, "email" => @regular_user.email}
		expect(response.status).to eq 403
	end
	#------------------------------------------------------------------------------------------------------------------------------------------------------------

	#report

	it"regular user shouldn't be able to open report of another user" do
		get "/api/users/#{@admin_user.id}/report", {:week => Date.today.to_s}, {"api-token" => @regular_user.api_token, "email" => @regular_user.email}
		expect(response.status).to eq 403
	end

	it"regular user should be able to open his own report" do
		get "/api/users/#{@regular_user.id}/report", {:week => Date.today.to_s}, {"api-token" => @regular_user.api_token, "email" => @regular_user.email}
		expect(response.status).to eq 200
	end

	it "admin should be able to open someone else's report" do
		get "/api/users/#{@regular_user.id}/report", {:week => Date.today.to_s}, {"api-token" => @admin_user.api_token, "email" => @admin_user.email}
		expect(response.status).to eq 200
	end

	it "if user doesn't pass accept header he'll get json response " do
		get "/api/users/#{@regular_user.id}/report", {:week =>  (Date.today - 1.month).to_s}, {"api-token" => @admin_user.api_token, "email" => @admin_user.email}
		expect(response.status).to eq 200
		expect(response['Content-Type']).to eq 'json'
	end

	it "if user pass accept header with 'application/pdf' value, he'll get content-type of 'application/pdf' " do
		get "/api/users/#{@regular_user.id}/report", {:week => Date.today.to_s}, {"Accept" => "application/pdf", "api-token" => @admin_user.api_token, "email" => @admin_user.email}
		expect(response.status).to eq 200
		expect(response['Content-Type']).to eq 'application/pdf'
	end
	#------------------------------------------------------------------------------------------------------------------------------------------------------------


end

