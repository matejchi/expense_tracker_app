require 'rails_helper'

RSpec.describe Api::V1::ExpensesController, type: :request do


	before(:all) do

		#roles
		@admin_role = FactoryGirl.create(:admin_role)
		@regular_role = FactoryGirl.create(:regular_role)

		#users
		@admin_user = FactoryGirl.create(:admin_user)
		@regular_user = FactoryGirl.create(:regular_user)

		#expenses
		@regular_user_expense = FactoryGirl.create(:expense) #today's expense
		@last_week_expense = FactoryGirl.create(:expense, :date => Date.today - 6.days)
		@last_month_expense = FactoryGirl.create(:expense, :date => Date.today - 29.days)
		@greater_than_100_expense = FactoryGirl.create(:expense, :amount => 300)
		@less_than_100_expense = FactoryGirl.create(:expense, :amount => 24)

		#filters
		@filter_all = FactoryGirl.create(:all_filter)
		@last_week = FactoryGirl.create(:last_week)
		@last_month = FactoryGirl.create(:last_month)
		@amount_les_then_100 = FactoryGirl.create(:amount_les_then_100)
		@amount_greater_than_or_equal_to_100 = FactoryGirl.create(:amount_greater_than_or_equal_to_100)
		@today = FactoryGirl.create(:today)
	end

	after(:all) do
		Role.destroy_all
		User.destroy_all
		Expense.destroy_all
		Filter.destroy_all
	end

	#index
	it "admin should be able to see all regular user's expenses" do
		get "/api/users/#{@regular_user.id}/expenses", nil, {"api-token" => @admin_user.api_token, "email" => @admin_user.email}
		expect(response.status).to eq 200
	end

	it "admin should be able to see all regular user's expenses" do
		get "/api/users/#{@regular_user.id}/expenses", nil, {"api-token" => @admin_user.api_token, "email" => @admin_user.email}
		returned_value = JSON.parse(response.body)
		expect(returned_value['expenses'].count).to eq 5
		expect(returned_value['expenses'][0]['expense']['amount']).to eq 200
		expect(returned_value['expenses'][0]['expense']['comment']).to eq 'bla bla bla'
		expect(returned_value['expenses'][0]['expense']['description']).to eq 'some description'
	end

	it "regular user should be able to see his expenses" do
		get "/api/users/#{@regular_user.id}/expenses", nil, {"api-token" => @regular_user.api_token, "email" => @regular_user.email}
		returned_value = JSON.parse(response.body)
		expect(returned_value['expenses'].count).to eq 5
		expect(returned_value['expenses'][0]['expense']['amount']).to eq 200
		expect(returned_value['expenses'][0]['expense']['comment']).to eq 'bla bla bla'
		expect(returned_value['expenses'][0]['expense']['description']).to eq 'some description'
		expect(response.status).to eq 200
	end

	it "regular user shouldn't be able to someone else's expenses" do
		@admin_expense = FactoryGirl.create(:expense, :user_id => @admin_user.id)
		get "/api/users/#{@admin_user.id}/expenses", nil, {"api-token" => @regular_user.api_token, "email" => @regular_user.email}
		expect(response.status).to eq 403
	end
	#------------------------------------------------------------------------------------------------------------------------------------------------------------

	#show
	it "admin should be able to see expense details" do
		get "/api/users/#{@regular_user.id}/expenses/#{@regular_user_expense.id}", nil, {"api-token" => @admin_user.api_token, "email" => @admin_user.email}
		returned_value = JSON.parse(response.body)
		expect(response.status).to eq 200
		expect(returned_value['expense']['amount']).to eq 200
		expect(returned_value['expense']['comment']).to eq 'bla bla bla'
		expect(returned_value['expense']['description']).to eq 'some description'
	end

	it "regular user should be able to see his own expense details" do
		get "/api/users/#{@regular_user.id}/expenses/#{@regular_user_expense.id}", nil, {"api-token" => @regular_user.api_token, "email" => @regular_user.email}
		returned_value = JSON.parse(response.body)
		expect(response.status).to eq 200
		expect(returned_value['expense']['amount']).to eq 200
		expect(returned_value['expense']['comment']).to eq 'bla bla bla'
		expect(returned_value['expense']['description']).to eq 'some description'
	end

	it "regular user shouldn't be able to see someone else's expense details" do
		@admin_expense = FactoryGirl.create(:expense, :user_id => @admin_user.id)
		get "/api/users/#{@regular_user.id}/expenses/#{@admin_expense.id}", nil, {"api-token" => @regular_user.api_token, "email" => @regular_user.email}
		expect(response.status).to eq 403
		expect(JSON.parse(response.body)["error"]).to eq "Access denied"
	end
	#------------------------------------------------------------------------------------------------------------------------------------------------------------

	#destroy
	it "admin should be able to delete his own expense" do
		@admin_expense = FactoryGirl.create(:expense, :user_id => @admin_user.id)
		get "/api/users/#{@admin_user.id}/expenses/#{@admin_expense.id}", nil, {"api-token" => @admin_user.api_token, "email" => @admin_user.email}
		expect(response.status).to eq 200
	end

	it "admin should be able to delete someone else's expense" do
		get "/api/users/#{@regular_user.id}/expenses/#{@regular_user_expense.id}", nil, {"api-token" => @admin_user.api_token, "email" => @admin_user.email}
		expect(response.status).to eq 200
	end

	it "regular user should be able to delete his own expense" do
		get "/api/users/#{@regular_user.id}/expenses/#{@regular_user_expense.id}", nil, {"api-token" => @regular_user.api_token, "email" => @regular_user.email}
		expect(response.status).to eq 200
	end

	it "regular user shouldn't be able to delete someone else's expense" do
		@admin_expense = FactoryGirl.create(:expense, :user_id => @admin_user.id)
		get "/api/users/#{@admin_user.id}/expenses/#{@admin_expense.id}", nil, {"api-token" => @regular_user.api_token, "email" => @regular_user.email}
		expect(response.status).to eq 403
	end
	#------------------------------------------------------------------------------------------------------------------------------------------------------------

	#create
	it "admin should be able to create his own expense" do
		@expense = {:description => 'admin expense', :amount => '120', :date => Time.now.to_date(), :time => Time.now.strftime("%H:%M"), :comment => "bla bla", :user_id => @admin_user.id}
		post "/api/users/#{@admin_user.id}/expenses", @expense, {"api-token" => @admin_user.api_token, "email" => @admin_user.email}
		returned_value = JSON.parse(response.body)
		expect(response.status).to eq 201
		expect(returned_value['expense']['description']).to eq @expense[:description]
		expect(returned_value['expense']['amount']).to eq @expense[:amount].to_f
		expect(returned_value['expense']['comment']).to eq @expense[:comment]
	end

	it "admin should be able to create someone else's expense" do
		@expense = {:description => 'some expense', :amount => '220', :date => Time.now.to_date(), :time => Time.now.strftime("%H:%M"), :comment => "bla", :user_id => @regular_user.id}
		post "/api/users/#{@regular_user.id}/expenses", @expense, {"api-token" => @admin_user.api_token, "email" => @admin_user.email}
		returned_value = JSON.parse(response.body)
		expect(response.status).to eq 201
		expect(returned_value['expense']['description']).to eq @expense[:description]
		expect(returned_value['expense']['amount']).to eq @expense[:amount].to_f
		expect(returned_value['expense']['comment']).to eq @expense[:comment]
	end

	it "regular user should be able to create his own expense" do
		@expense = {:description => 'expense', :amount => '2290', :date => Time.now.to_date(), :time => Time.now.strftime("%H:%M"), :comment => "bla", :user_id => @regular_user.id}
		post "/api/users/#{@regular_user.id}/expenses", @expense, {"api-token" => @regular_user.api_token, "email" => @regular_user.email}
		returned_value = JSON.parse(response.body)
		expect(response.status).to eq 201
		expect(returned_value['expense']['description']).to eq @expense[:description]
		expect(returned_value['expense']['amount']).to eq @expense[:amount].to_f
		expect(returned_value['expense']['comment']).to eq @expense[:comment]
	end

	it "regular user shouldn't be able to create expense for someone else" do
		@expense = {:description => 'trying to create expense', :amount => '20000', :date => Time.now.to_date(), :time => Time.now.strftime("%H:%M"), :comment => "bla", :user_id => @admin_user.id}
		post "/api/users/#{@admin_user.id}/expenses", @expense, {"api-token" => @regular_user.api_token, "email" => @regular_user.email}
		expect(response.status).to eq 403
	end

	#------------------------------------------------------------------------------------------------------------------------------------------------------------

	#update
	it "admin should be able to update his own expense" do
		@new_expense = {:description => 'admin expense', :amount => '120', :date => Time.now.to_date(), :time => Time.now.strftime("%H:%M"), :comment => "bla bla", :user_id => @admin_user.id}
		post "/api/users/#{@admin_user.id}/expenses", @new_expense, {"api-token" => @admin_user.api_token, "email" => @admin_user.email}
		post_returned_value = JSON.parse(response.body)

		put "/api/users/#{@admin_user.id}/expenses/#{post_returned_value['expense']['id']}", {:amount => post_returned_value['expense']['amount'].to_f + 20.0 }, {"api-token" => @admin_user.api_token, "email" => @admin_user.email}
		returned_value = JSON.parse(response.body)
		expect(response.status).to eq 200
		expect(returned_value['expense']['amount']).to eq 140
		expect(returned_value['expense']['comment']).to eq @new_expense[:comment]
	end

	it "admin should be able to update someone else's expense" do
		get "/api/users/#{@regular_user.id}/expenses/1", nil,  {"api-token" => @admin_user.api_token, "email" => @admin_user.email}
		get_returned_value = JSON.parse(response.body)
		expect(get_returned_value['expense']['amount']).to eq 200

		put "/api/users/#{@regular_user.id}/expenses/1", {:amount => 300}, {"api-token" => @admin_user.api_token, "email" => @admin_user.email}
		returned_value = JSON.parse(response.body)
		expect(response.status).to eq 200
		expect(returned_value['expense']['amount']).to eq 300
	end

	it "regular user should be able to update his own expense" do
		get "/api/users/#{@regular_user.id}/expenses/1", nil,  {"api-token" => @regular_user.api_token, "email" => @regular_user.email}
		get_returned_value = JSON.parse(response.body)
		expect(get_returned_value['expense']['amount']).to eq 200

		put "/api/users/#{@regular_user.id}/expenses/1", {:amount => 500}, {"api-token" => @regular_user.api_token, "email" => @regular_user.email}
		returned_value = JSON.parse(response.body)
		expect(response.status).to eq 200
		expect(returned_value['expense']['amount']).to eq 500
	end

	it "regular user shouldn't be able to update someone else's expense" do
		@admin_expense = FactoryGirl.create(:expense, :user_id => @admin_user.id)
		put "/api/users/#{@admin_user.id}/expenses/2", {:amount => 1000}, {"api-token" => @regular_user.api_token, "email" => @regular_user.email}
		expect(response.status).to eq 403
	end
	#------------------------------------------------------------------------------------------------------------------------------------------------------------

	#filter
	it "regular user shouldn't be able to filter someone else's expenses" do
		@admin_expense = FactoryGirl.create(:expense, :user_id => @admin_user.id)
		get "/api/users/#{@admin_user.id}/expenses/filter", {:type => @filter_all.id}, {"api-token" => @regular_user.api_token, "email" => @regular_user.email}
		expect(response.status).to eq 403
	end

	it "regular user should be able to filter his own expenses" do
		get "/api/users/#{@regular_user.id}/expenses/filter", {:type => @filter_all.id}, {"api-token" => @regular_user.api_token, "email" => @regular_user.email}
		expect(response.status).to eq 200
	end

	it "admin should be able to filter someone else's expenses" do
		get "/api/users/#{@regular_user.id}/expenses/filter", {:type => @filter_all.id}, {"api-token" => @admin_user.api_token, "email" => @admin_user.email}
		expect(response.status).to eq 200
	end

	it "should return all expenses" do
		get "/api/users/#{@regular_user.id}/expenses/filter", {:type => @filter_all.id}, {"api-token" => @regular_user.api_token, "email" => @regular_user.email}
		expect(response.status).to eq 200
		returned_value = JSON.parse(response.body)
		expect(returned_value['expenses'].count).to eq 5
	end

	it "should return last week's expenses" do
		get "/api/users/#{@regular_user.id}/expenses/filter", {:type => @last_week.id}, {"api-token" => @regular_user.api_token, "email" => @regular_user.email}
		expect(response.status).to eq 200
		returned_value = JSON.parse(response.body)
		expect(returned_value['expenses'].count).to eq 1
		expect(returned_value['expenses'][0]['expense']['date']).to eq @last_week_expense.date.to_s
	end

	it "should return last month's expenses" do
		get "/api/users/#{@regular_user.id}/expenses/filter", {:type => @last_month.id}, {"api-token" => @regular_user.api_token, "email" => @regular_user.email}
		expect(response.status).to eq 200
		returned_value = JSON.parse(response.body)

		expect(returned_value['expenses'].count).to eq 2
		expect(returned_value['expenses'][0]['expense']['date']).to eq @last_month_expense.date.to_s
	end

	it "should return all expenses greater than 100" do
		get "/api/users/#{@regular_user.id}/expenses/filter", {:type => @amount_greater_than_or_equal_to_100.id}, {"api-token" => @regular_user.api_token, "email" => @regular_user.email}
		expect(response.status).to eq 200
		returned_value = JSON.parse(response.body)
		expect(returned_value['expenses'].count).to eq 4
	end

	it "should return all expenses less than 100" do
		get "/api/users/#{@regular_user.id}/expenses/filter", {:type => @amount_les_then_100.id}, {"api-token" => @regular_user.api_token, "email" => @regular_user.email}
		expect(response.status).to eq 200
		returned_value = JSON.parse(response.body)
		expect(returned_value['expenses'].count).to eq 1
	end

	it "should return today's expenses" do
		get "/api/users/#{@regular_user.id}/expenses/filter", {:type => @today.id}, {"api-token" => @regular_user.api_token, "email" => @regular_user.email}
		expect(response.status).to eq 200
		returned_value = JSON.parse(response.body)
		expect(returned_value['expenses'].count).to eq 3
		expect(returned_value['expenses'][0]['expense']['date']).to eq @regular_user_expense.date.to_s
	end
	#------------------------------------------------------------------------------------------------------------------------------------------------------------

end