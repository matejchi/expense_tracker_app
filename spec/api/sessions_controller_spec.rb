require 'rails_helper'

RSpec.describe Api::V1::SessionsController, type: :request do

	before(:all) do
		@admin_role = FactoryGirl.create(:admin_role)
		@regular_role = FactoryGirl.create(:regular_role)
		@admin_user = FactoryGirl.create(:admin_user)
		@regular_user = FactoryGirl.create(:regular_user)
	end

	after(:all) do
		Role.destroy_all
		User.destroy_all
	end

	#create
	it "user shouldn't be able to login if he pass the wrong email" do
		post "/api/sessions", {:email => 'someone@isp.net', :password => "0000"}
		expect(response.status).to eq 401
	end

	it "user shouldn't be able to login if he pass wrong password" do
		post "/api/sessions", {:email => 'user@isp.net', :password => "0100"}
		expect(response.status).to eq 401
	end

	it "user should be able to login if he pass correct email and password" do
		post "/api/sessions", {:email => 'user@isp.net', :password => "0000"}
		expect(response.status).to eq 201
	end

	it "if user that is logging in is admin he should get admin role after successull login" do
		post "/api/sessions", {:email => 'admin@isp.net', :password => "root"}
		returned_value = JSON.parse(response.body)
		expect(returned_value['user']['role']).to eq "admin"
	end
	#------------------------------------------------------------------------------------------------------------------------------------------------------------

	#destroy

	it "if user is logging out without proper headers he should receive status 401" do
		post "/api/sessions", {:email => 'user@isp.net', :password => "0000"}
		expect(response.status).to eq 201

		returned_value = JSON.parse(response.body)

		expect(returned_value['user']['email']).to eq 'user@isp.net'

		fake_token = "bl4bl4%th1s1sMyt0k3n"
		expect(returned_value['user']['api_token']).not_to eq fake_token

		delete "/api/sessions/#{returned_value['user']['id']}", nil, {"api-token" => fake_token, "email" => returned_value['user']['email']}
		expect(response.status).to eq 401
	end

	it "if user is logging out with proper headers he should receive status 200" do
		post "/api/sessions", {:email => 'user@isp.net', :password => "0000"}
		expect(response.status).to eq 201

		returned_value = JSON.parse(response.body)

		expect(returned_value['user']['email']).to eq 'user@isp.net'

		delete "/api/sessions/#{returned_value['user']['id']}", nil, {"api-token" => returned_value['user']['api_token'], "email" => returned_value['user']['email']}
		expect(response.status).to eq 200
	end
	#------------------------------------------------------------------------------------------------------------------------------------------------------------



end