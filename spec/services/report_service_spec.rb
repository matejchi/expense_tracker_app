require 'rails_helper'

RSpec.describe ReportService do

	before(:all) do

		#roles
		@fake_role = FactoryGirl.create(:admin_role)
		@regular_role = FactoryGirl.create(:regular_role)

		#users
		@fake_user = FactoryGirl.create(:admin_user)
		@user = FactoryGirl.create(:regular_user)

		#expenses
		@expense = FactoryGirl.create(:expense)
	end

	after(:all) do
		Role.destroy_all
		User.destroy_all
		Expense.destroy_all
	end

	it "should return total of 200" do
		params = {:week => Date.today.to_s}
		result = ReportService.generate_report_data(params, @user)
		expect(result.last).to eq("Total: 200.0")
	end

	it "should return average of 200" do
		FactoryGirl.create(:expense, :date => (Date.today - 15.days).to_s )
		FactoryGirl.create(:expense, :date => (Date.today - 13.days).to_s )
		params = {:week => (Date.today - 14.days).to_s}
		result = ReportService.generate_report_data(params, @user)
		expect(result[result.size-2]).to eq("Average: 200.0")
	end

	it "should return message" do
		params = {:week => (Date.today - 1.month).to_s }
		result = ReportService.generate_report_data(params, @user)
		expect(result[0]).to start_with("There is no")
	end

	it "should return nil if param is invalid" do
		params = {:week => "asdads"}
		result = ReportService.generate_report_data(params, @user)
		expect(result).to eq nil
	end

	it "check_date() should return false if param name is not correct" do
		params = {:some_weird_param => "bla bla"}
		result = ReportService.check_date(params)
		expect(result).to be false
	end

	it "check_date() should catch exception and return false if param is not correctly formated" do
		params = {:week => "2011-2222"}
		result = ReportService.check_date(params)
		expect(result).to be false
	end

end

