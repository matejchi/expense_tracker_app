
require 'rails_helper'

RSpec.describe ExpensesFilterService do

	before(:all) do
		#role
		@admin_role = FactoryGirl.create(:admin_role)
		@regular_role = FactoryGirl.create(:regular_role)

		#user
		@admin_user = FactoryGirl.create(:admin_user)
		@regular_user = FactoryGirl.create(:regular_user)

		#expenses
		@regular_user_expense = FactoryGirl.create(:expense) #today's expense
		@last_week_expense = FactoryGirl.create(:expense, :date => Date.today - 6.days)
		@last_month_expense = FactoryGirl.create(:expense, :date => Date.today - 29.days)
		@greater_than_100_expense = FactoryGirl.create(:expense, :amount => 300)
		@less_than_100_expense = FactoryGirl.create(:expense, :amount => 24)

		#filters
		@filter_all = FactoryGirl.create(:all_filter)
		@last_week = FactoryGirl.create(:last_week)
		@last_month = FactoryGirl.create(:last_month)
		@amount_les_then_100 = FactoryGirl.create(:amount_les_then_100)
		@amount_greater_than_or_equal_to_100 = FactoryGirl.create(:amount_greater_than_or_equal_to_100)
		@today = FactoryGirl.create(:today)
	end

	after(:all) do
		Role.destroy_all
		User.destroy_all
		Expense.destroy_all
		Filter.destroy_all
	end

	it "should return nil for wrong filter id" do
		result = ExpensesFilterService.get_filter_results("blabla", @regular_user)
		expect(result).to eq nil
	end

	it "should return 2 'last_week' expenses" do
		FactoryGirl.create(:expense, :date => Date.today - 4.days)
		result = ExpensesFilterService.get_filter_results(@last_week.id.to_s, @regular_user)
		expect(result.count).to eq 2
	end

	it "should return 3 'last_month' expenses" do
		FactoryGirl.create(:expense, :date => Date.today - 29.days)
		result = ExpensesFilterService.get_filter_results(@last_month.id.to_s, @regular_user)
		expect(result.count).to eq 3
	end

	it "should return all expenses" do
		result = ExpensesFilterService.get_filter_results(@filter_all.id.to_s, @regular_user)
		expect(result.count).to eq 5
	end

	it "should return 1 'les than 100' expense" do
		result = ExpensesFilterService.get_filter_results(@amount_les_then_100.id.to_s, @regular_user)
		expect(result.count).to eq 1
	end

	it "should return 4 'greater than or equal to 100' expenses" do
		result = ExpensesFilterService.get_filter_results(@amount_greater_than_or_equal_to_100.id.to_s, @regular_user)
		expect(result.count).to eq 4
	end

	it "should return 3 today's expenses" do
		result = ExpensesFilterService.get_filter_results(@today.id.to_s, @regular_user)
		expect(result.count).to eq 3
	end

	it "should return empty for non-existing user" do
		result = ExpensesFilterService.get_filter_results(@today.id.to_s, @admin_user)
		expect(result.present?).to be_falsey
	end

end

