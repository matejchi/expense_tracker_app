require 'rails_helper'

RSpec.describe SessionService do

	it "should return false if params are wrong or missing" do
		params = {:wrong_email_param => "user@isp.net", :wrong_password_param => "0000"}
		result = SessionService.login_params_valid?(params)
		expect(result).to eq false

		params = {}
		result = SessionService.login_params_valid?(params)
		expect(result).to eq false
	end

	it "should return false if password is too small or too big" do
		params = {:email => "user@isp.net", :password => "123"}
		result = SessionService.login_params_valid?(params)
		expect(result).to eq false

		params = {:email => "user@isp.net", :password => "1234567890abcefgh"}
		result = SessionService.login_params_valid?(params)
		expect(result).to eq false
	end

	it "should return false if email is not formatted correctly" do
		params = { :email => "adaddddd", :password => "0000"}
		result = SessionService.login_params_valid?(params)
		expect(result).to eq false
	end

	it "should return false if email is not formatted correctly" do
		params = { :email => "adad@dddd", :password => "0000"}
		result = SessionService.login_params_valid?(params)
		expect(result).to eq false
	end

	it "should return false if email is not formatted correctly" do
		params = { :email => "adad@ddd.d", :password => "0000"}
		result = SessionService.login_params_valid?(params)
		expect(result).to eq false
	end
end

