FactoryGirl.define do
	factory :filter do

		factory :all_filter do
			name "All"
		end

		factory :last_week do
			name "Last Week"
		end

		factory :last_month do
			name "Last Month"
		end

		factory :amount_les_then_100 do
			name "Amount < 100"
		end

		factory :amount_greater_than_or_equal_to_100 do
			name "Amount >= 100"
		end

		factory :today do
			name "Today"
		end

	end
end