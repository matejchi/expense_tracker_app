FactoryGirl.define do

	factory :user do

		factory :admin_user do
			first_name "Big"
			last_name "Boss"
			email "admin@isp.net"
			password "root"
			role_id 1
		end

		factory :regular_user do
			first_name "Regular"
			last_name "User"
			email "user@isp.net"
			password "0000"
			role_id 2
		end

	end

end
