FactoryGirl.define do
	factory :role do

		factory :admin_role do
			name "admin"
		end

		factory :regular_role do
			name "regular"
		end

	end
end
