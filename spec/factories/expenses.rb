FactoryGirl.define do

	factory :expense do
		description 'some description'
		amount 200.00
		date Time.now.to_date
		time Time.now.strftime("%H:%M")
		comment 'bla bla bla'
		user_id 2
	end

end
