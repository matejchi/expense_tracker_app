
require 'rails_helper'

RSpec.describe Expense, type: :model do


	it "should validate presence of description" do
		expense = FactoryGirl.build(:expense, :description => '')
		expense.valid?
		expect(expense.errors).to have_key(:description)
	end

	it "should validate presence of amount" do
		expense = FactoryGirl.build(:expense, :amount => '')
		expense.valid?
		expect(expense.errors).to have_key(:amount)
	end

	it "should validate presence of date" do
		expense = FactoryGirl.build(:expense, :date => nil)
		expense.valid?
		expect(expense.errors).to have_key(:date)
	end

	it "should validate presence of time" do
		expense = FactoryGirl.build(:expense, :time => nil)
		expense.valid?
		expect(expense.errors).to have_key(:time)

	end

	it "should validate presence of comment" do
		expense = FactoryGirl.build(:expense, :comment => nil)
		expense.valid?
		expect(expense.errors).to have_key(:comment)

	end

	it "should validate that amount is number" do
		expense = FactoryGirl.build(:expense, :amount => "some text")
		expense.valid?
		expect(expense.errors).to have_key(:amount)
	end

	it "should create expense" do
		expense = FactoryGirl.create(:expense)
		expense.valid?
		expect(expense.errors.present?).to be false
		expect(expense.description).to eq "some description"
		expect(expense.amount).to eq 200
		expect(expense.date).to eq Time.now.to_date
		expect(expense.comment).to eq "bla bla bla"
	end

	it "should have one associated user record" do
		role = FactoryGirl.create(:regular_role)
		user = FactoryGirl.create(:regular_user)
		expense = FactoryGirl.create(:expense, :user_id => 1)

		expect(expense).to respond_to(:user)
		expect(expense.user.email).to eq "user@isp.net"
	end


end

