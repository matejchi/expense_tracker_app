require 'rails_helper'

RSpec.describe Role, type: :model do

	it "should validate presence of name" do
		role = Role.create(:name => '')
		role.valid?
		expect(role.errors).to have_key(:name)

		another_role = FactoryGirl.create(:regular_role)
		another_role.valid?
		expect(another_role.errors.present?).to be false
	end


	it "should have one associated user record" do
		role = FactoryGirl.create(:regular_role)
		user = FactoryGirl.create(:regular_user)

		expect(role).to respond_to(:users)
		expect(role.users.first.email).to eq "user@isp.net"
	end

end