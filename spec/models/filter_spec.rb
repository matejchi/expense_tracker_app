
require 'rails_helper'

RSpec.describe Filter, type: :model do

	it "should create filter" do
		filter = FactoryGirl.create(:last_week)
		expect(filter.errors.present?).to be false
		expect(filter.name).to eq "Last Week"
	end

	it "should validate presence of name" do
		filter = Filter.create(:name => '')
		filter.valid?
		expect(filter.errors).to have_key(:name)

		another_filter = FactoryGirl.create(:last_month)
		another_filter.valid?
		expect(another_filter.errors.present?).to be false
	end

end