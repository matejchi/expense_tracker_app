
 var home_module = angular.module("home", ["ui.router", "ui.bootstrap"]);

 home_module.config(["$stateProvider", function homeConfig($stateProvider) {

	$stateProvider.state("home", {
		url: "/home",
		templateUrl: "home/home.tpl.html",
		controller: "HomeController"
	});

 }]);

home_module.controller("HomeController", ["$scope", function HomeController($scope) {
	console.log("HomeController");

	$scope.current_user = JSON.parse(sessionStorage.getItem("current_user"));
}]);